import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GridSearchImpl implements GridSearch{
    int height;
    int width;
    char[][] grid;

    // Finds the height of the grid from the first line of the file
    public int findHeight(String filePath){
        // Reads the file's first line
        try (BufferedReader lineOne = new BufferedReader(new FileReader(filePath))) {
            String size = lineOne.readLine();
            // Takes the int before the 'x' character as the height
            height = Integer.parseInt(size.substring(0, size.indexOf("x")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return height;
    }
    // Finds the width of the grid from the first line of the file
    public int findWidth(String filePath){
        // Reads the file's first line
        try (BufferedReader lineOne = new BufferedReader(new FileReader(filePath))) {
            String size = lineOne.readLine();
            // Takes the int after the 'x' character as the width
            width = Integer.parseInt(size.substring(size.indexOf("x") + 1));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return width;
    }
    // Builds the Grid that the word search is taking pace in
    public char[][] buildGrid(String filePath){
        String nextLine;
        try (BufferedReader lineOne = new BufferedReader(new FileReader(filePath))) {
            String size = lineOne.readLine();
            height = findHeight(filePath);
            width = findWidth(filePath);
            grid = new char[height][width];
            //for the height of the grid remove all spaces from the grid line by line
            for (int i = 0; i < height; i++) {
                nextLine = lineOne.readLine();
                if (nextLine != null) {
                    grid[i] = nextLine.replace(" ", "").toCharArray();
                } else {
                    System.out.println("WRONG HEIGHT!");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return grid;
    }
    //finds if there is a word to the right by setting the next point to search for in the word to the Right
    public String findWordRight(Point start, Point end) {
        if (end.y > width) {
            return "";
        }
        if (start.y > end.y) {
            return "";
        }
        return grid[start.x][start.y] + findWordRight(new Point(start.x, start.y + 1), end);
    }

    //finds if there is a word to the right by setting the next point to search for in the word to the Left
    public String findWordLeft(Point start, Point end) {
        if (end.y < 0) {
            return "";
        }
        if (start.y < end.y) {
            return "";
        }
        return grid[start.x][start.y] + findWordLeft(new Point(start.x, start.y - 1), end);
    }

    //finds if there is a word to the right by setting the next point to search for in the word Up
    public String findWordUp(Point start, Point end) {
        if (end.x > height) {
            return "";
        }
        if (start.x > end.x) {
            return "";
        }
        return grid[start.x][start.y] + findWordUp(new Point(start.x + 1, start.y), end);
    }

    //finds if there is a word to the right by setting the next point to search for in the word Down
    public String findWordDown(Point start, Point end) {
        if (end.x < 0) {
            return "";
        }
        if (start.x < end.x) {
            return "";
        }
        return grid[start.x][start.y] + findWordDown(new Point(start.x - 1, start.y), end);
    }

    //finds if there is a word to the right by setting the next point to search for in the word Up and Right
    public String findWordUpRight(Point start, Point end) {
        if (end.x > height || end.y > width) {
            return "";
        }
        if (start.x > end.x && start.y > end.y) {
            return "";
        }
        return grid[start.x][start.y] + findWordUpRight(new Point(start.x + 1, start.y + 1), end);
    }

    //finds if there is a word to the right by setting the next point to search for in the word Up and Left
    public String findWordUpLeft(Point start, Point end) {
        if (end.x > height || end.y < 0) {
            return "";
        }
        if (start.x > end.x && start.y < end.y) {
            return "";
        }
        return grid[start.x][start.y] + findWordUpLeft(new Point(start.x + 1, start.y - 1), end);
    }


    //finds if there is a word to the right by setting the next point to search for in the word Down and Right
    public String findWordDownRight(Point start, Point end) {
        if (end.x < 0 || end.y > width) {
            return "";
        }
        if (start.x < end.x && start.y > end.y) {
            return "";
        }
        return grid[start.x][start.y] + findWordDownRight(new Point(start.x - 1, start.y + 1), end);
    }

    //finds if there is a word to the right by setting the next point to search for in the word Down and Left
    public String findWordDownLeft(Point start, Point end) {
        if (end.x < 0 || end.y < 0) {
            return "";
        }
        if (start.x < end.x && start.y < end.y) {
            return "";
        }
        return grid[start.x][start.y] + findWordDownLeft(new Point(start.x - 1, start.y - 1), end);
    }
}
