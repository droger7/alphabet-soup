import java.io.*;
import java.util.*;
import java.awt.Point;

public class WordFind extends GridSearchImpl{
    private List<String> searchWords;
    private String foundWord;
    private Point foundWordStart;
    private Point foundWordEnd;

    // Set the searchGrid as well as the searchWords to be found in the array list searchWords
    WordFind(String filePath) throws IOException {
        searchWords = new ArrayList<>();
        foundWordStart = new Point();
        foundWordEnd = new Point();
        String nextLine;
        // Read file
        try (BufferedReader lineOne = new BufferedReader(new FileReader(filePath))) {
            String size = lineOne.readLine();
            buildGrid(filePath);
            nextLine = lineOne.readLine();
            // Iterate until there are no more lines to be read
            while (nextLine != null) {
                searchWords.add(nextLine);
                nextLine = lineOne.readLine();

            }
        }
    }

    // Primary function to be called in main
    public void searchForWord() {
        for (String searchWord : searchWords) {
            char searchChar = searchWord.charAt(0);
            // For the height and width of the grid loop until searchChar is found then set the location
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (grid[i][j] == searchChar) {
                        foundWordStart.x = i;
                        foundWordStart.y = j;
                        findWord(searchWord);
                        displayGridCoordinates(searchWord);
                    }
                }
            }
        }
    }
    // Displays the word that was found as well as the starting and end coordinates
    private void displayGridCoordinates(String searchWord){
        if (searchWord.equals(foundWord)) {
            System.out.println(foundWord + " " + foundWordStart.x + ":" + foundWordStart.y +
                    " " + foundWordEnd.x + ":" + foundWordEnd.y);
            foundWord = "";
            foundWordEnd = new Point();
            foundWordStart = new Point();
        }
    }

    //finds the direction of the word once a searchChar has been found
    private void findWord(String searchWord) {
        char nextChar = searchWord.charAt(1);
        int searchWordLength = searchWord.length() -1 ;

        try {
            // Try to find if nextChar is to the right of the searchChar, if so continue till searchWordLength
            if (grid[foundWordStart.x][foundWordStart.y + 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x, foundWordStart.y + searchWordLength);
                foundWord = findWordRight(foundWordStart, foundWordEnd);

                return;
            }
            //will go out of bounds reading nextChar if the letter is on the edge of the grid
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            // Try to find if nextChar is to the left of the searchChar, if so continue till searchWordLength
            if (grid[foundWordStart.x][foundWordStart.y - 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x, foundWordStart.y - searchWordLength);
                foundWord = findWordLeft(foundWordStart, foundWordEnd);
                return;
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            // Try to find if nextChar is Up from the searchChar, if so continue till searchWordLength
            if (grid[foundWordStart.x + 1][foundWordStart.y] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x + searchWordLength, foundWordStart.y);
                foundWord = findWordUp(foundWordStart, foundWordEnd);
                return;
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            // Try to find if nextChar is Down from the searchChar, if so continue till searchWordLength
            if (grid[foundWordStart.x - 1][foundWordStart.y] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x - searchWordLength, foundWordStart.y);
                foundWord = findWordDown(foundWordStart, foundWordEnd);
                return;
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            // Try to find if nextChar is Up and Right from the searchChar, if so continue till searchWordLength
            if (grid[foundWordStart.x + 1][foundWordStart.y + 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x + searchWordLength, foundWordStart.y + searchWordLength);
                foundWord = findWordUpRight(foundWordStart, foundWordEnd);
                return;
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            // Try to find if nextChar is Up and Left from the searchChar, if so continue till searchWordLength
            if (grid[foundWordStart.x + 1][foundWordStart.y - 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x + searchWordLength, foundWordStart.y - searchWordLength);
                foundWord = findWordUpLeft(foundWordStart, foundWordEnd);
                return;
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            // Try to find if nextChar is Down and Right from the searchChar, if so continue till searchWordLength
            if (grid[foundWordStart.x - 1][foundWordStart.y + 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x - searchWordLength, foundWordStart.y + searchWordLength);
                foundWord = findWordDownRight(foundWordStart, foundWordEnd);
                return;
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            // Try to find if nextChar is Down and Left from the searchChar, if so continue till searchWordLength
            if (grid[foundWordStart.x - 1][foundWordStart.y - 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x - searchWordLength, foundWordStart.y - searchWordLength);
                foundWord = findWordDownLeft(foundWordStart, foundWordEnd);
                return;
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        foundWordStart = new Point();
        foundWordEnd = new Point();
    }
}


