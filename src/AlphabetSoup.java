/*

David Rogers
March 4, 2019

This code was developed for the Enlighten IT code challenge.

Here is the format of our input file (test.txt):
5x5
H A S D F
G E Y B H
J K L Z X
C V B L N
G O O D O
HELLO
GOOD
BYE

Here is the format of our output:
HELLO 0:0 4:4
GOOD 4:0 4:3
BYE 1:3 1:1

*/

import java.io.*;
import java.util.*;
public class AlphabetSoup {


    public static void main(String[] args) {
        String filePath;
        do {
            System.out.print("Enter a File Path of the Word Search: ");
            Scanner in = new Scanner(System.in);
            filePath = in.nextLine();
            try {
                WordFind alphabetSoup = new WordFind(filePath);

               alphabetSoup.searchForWord();
            } catch (IOException e) {
                System.out.print("Not a file! \n");
            }
        } while (!filePath.contains(".txt"));

    }
}
