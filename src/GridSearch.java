import java.awt.*;

public interface GridSearch {
    int findHeight(String filePath);
    int findWidth(String filePath);
    char[][] buildGrid(String filePath);
    String findWordRight(Point start, Point end);
    String findWordLeft(Point start, Point end);
    String findWordUp(Point start, Point end);
    String findWordDown(Point start, Point end);
    String findWordUpRight(Point start, Point end);
    String findWordUpLeft(Point start, Point end);
    String findWordDownRight(Point start, Point end);
    String findWordDownLeft(Point start, Point end);
}
